      var listaCiudades=[
         {
            nombre:'Paris',
            poblacion: '2 241 346 habitantes',
            imagen: 'img/paris3.jpg',
            informacion: 'París es la capital de Francia y una de las grandes ciudades europeas. Es para muchos el destino turístico más romántico y popular de todo el planeta. París es una de las ciudades más visitadas del mundo, además de ser cuna de algunos movimientos vanguardistas. Su larga historia y su rico patrimonio cultural, gastronómico y social hace que sea el destino preferido por miles de turistas, atraídos por fantásticos monumentos',
         },
         {
            nombre:'Budapest',
            poblacion: '1 757 600 habitantes',
            imagen: 'img/budapest-panoramica.jpg',
            informacion: 'Budapest, capital de Hungría, es la ciudad más importante del país y se ha convertido por méritos propios en una de las principales ciudades turísticas de Europa.',
         },
         {
            nombre:'Monterrey',
            poblacion:'4 437 643 habitantes',
            imagen: 'img/monterrey3.jpg',
            informacion: '  Monterrey, capital del Estado de Nuevo León, es una de las ciudades más importantes de México y se encuentra ubicada en el noreste del país. Está situada junto a la Sierra Madre Oriental, cuya principal montaña es el Cerro de "La Silla", símbolo característico de nuestra ciudad.',
         },
         {
            nombre:'Tokio',
            poblacion:'39 643 000 habitantes',
            imagen: 'img/tokio2.jpg',
            informacion: 'Tokio, la capital de Japón es una de las ciudades mas espectaculares de la región de Asia-Pacífico, aún con esa cantidad de habitantes, es una ciudad limpia, ordenada, de medios de transporte eficientes y lo mejor de todo, absolutamente segura.',
         },
         {
            nombre: 'Berlin',
            poblacion:'3 470 000 habitantes',
            imagen: 'img/berlin.jpg',
            informacion: 'Berlín es la ciudad capital de la República Federal de Alemania y una de las ciudades más bellas, liberales y cosmopolitas del mundo. En Berlín se pueden encontrar algunas de las mejores infraestructuras del mundo en lo referente al transporte, vivienda y la recreación.',
         },
         {
            nombre: 'Barcelona',
            poblacion:'1 609 000 habitantes',
            imagen: 'img/barcelona3.jpg',
            informacion: ' Se encuentra situada al noreste de España, muy cerca de Francia, es la segunda ciudad más poblada de España, por detrás de Madrid. En Barcelona existen dos idiomas oficiales, el castellano y el catalán.',
         },
      ]


      var boton = document.querySelector('.side-drawer');
      var desplazar=document.querySelector('.navigation-drawer')
             
      boton.addEventListener('click', function(){
            
         desplazar.classList.toggle('open-drawer');
             
      });
      

      var btn_ocultar = document.querySelector('.side-drawer-b')
      var ocultar=document.querySelector('.navigation-drawer')
      
      
      btn_ocultar.addEventListener('click',function(){
        
            ocultar.classList.toggle('close-drawer')
      })


      /***************************************************************/
      var body = document.getElementsByTagName('body')[0]
      //ITERACION O TAMBIEN LLAMADO "FOR LOOP"
      for(var i = 0; i < listaCiudades.length; i++){
         //OBTENER UN SOLO ELEMENTO DE LA LISTA CON SU INDICE
         var ciudad = listaCiudades[i];

         //alert(ciudad.nombre);
         //crear contenedor
         var section=document.createElement('section')
         section.setAttribute('id',ciudad.nombre)
         section.style= 'background-image: url(' + ciudad.imagen + ')'

         //crear titulo
         var titulo=document.createElement('h2')
         titulo.innerHTML= ciudad.nombre

         //crear parrafo
         var parrafo =document.createElement('p')
         parrafo.innerHTML = ciudad.poblacion
     

         //Añadir elementos

         section.appendChild(titulo)
         section.appendChild(parrafo)
         body.appendChild(section)


      }
















