var listaCiudades= [
    {
        nombre:'Google Chrome',
        descripcion: 'Google Chrome es un navegador web de software privativo, desarrollado por Google, aunque derivado de proyectos de código abierto (como el motor de renderizado Blink).',
        imagen: 'img/fondo-chrome.jpg',
        imagen_navegador: 'img/logo-chrome.png'
    },
    {
        nombre:'Firefox',
        descripcion: 'es un navegador web libre y de código abierto desarrollado para Linux, Android, IOS, OS X y Microsoft Windows y coordinado por la Corporación Mozilla y la Fundación Mozilla.  Usa el motor Gecko para renderizar páginas web, el cual implementa actuales y futuros estándares web.',
        imagen: 'img/fondo-firefox.jpg',
        imagen_navegador: 'img/logo-firefox.png',
    },
    {
        nombre:'Opera',
        descripcion:'Opera es un navegador web creado por la empresa noruega Opera Software. Usa el motor de renderizado Blink. Tiene versiones para computadoras de escritorio, teléfonos móviles y tabletas.',
        imagen: 'img/fondo-opera.jpg',
        imagen_navegador: 'img/logo-opera.png',
    },
    {
        nombre:'Safari',
        descripcion:'Safari es un navegador web de código cerrado desarrollado por Apple Inc. Está disponible para macOS, iOS (el sistema usado por el iPhone, el iPod touch y iPad). Safari es más veloz y consume menos energía que otros navegadores',
        imagen: 'img/safari.png',
        imagen_navegador: 'img/safari-logo.png',
    },
    {
        nombre: 'Internet Explorer',
        descripcion:'Es un navegador web desarrollado por Microsoft para el sistema operativo Microsoft Windows.',
        imagen: 'img/fondo-explorer.jpg',
        imagen_navegador: 'img/logo-explorer.png',
    },
   /* {
        nombre: 'Tor',
        descripcion:'Tor es la sigla de The Onion Router (en español: Enrutador de Cebolla), Es un proyecto cuyo objetivo principal es el desarrollo de una red de comunicaciones distribuida de baja latencia y superpuesta sobre internet',
        imagen: 'img/deep.jpg',
        imagen_navegador: 'img/logo-tor2.png',
    },*/
]
      
 /***************************************************************/
var body = document.getElementsByTagName('body')[0]
//ITERACION O TAMBIEN LLAMADO "FOR LOOP"
for(var i = 0; i < listaCiudades.length; i++){
    //OBTENER UN SOLO ELEMENTO DE LA LISTA CON SU INDICE
    var ciudad = listaCiudades[i];

    //alert(ciudad.nombre);
    //crear contenedor
    var section=document.createElement('section')
    section.setAttribute('id',ciudad.nombre)
    section.style= 'background-image: url(' + ciudad.imagen + ')'
    //crear titulo
    var titulo=document.createElement('h2')
    titulo.innerHTML= ciudad.nombre
    //crear parrafo
    var div=document.createElement('div')
    
    var parrafo =document.createElement('p')
    parrafo.innerHTML = ciudad.descripcion
              
    var imagen =document.createElement('img')
    imagen.setAttribute('src',ciudad.imagen_navegador)
    //imagen.innerHTML = ciudad.imagen_navegador
     
    //Añadir elementos
    body.appendChild(section)
    section.appendChild(titulo)
    div.appendChild(parrafo)
    section.appendChild(imagen)
    section.appendChild(div)


}
















