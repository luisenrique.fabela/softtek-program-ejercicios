function Vehiculo(auto,marca,nombre,velocidad,  opciones){
    this.auto=auto,
    this.marca=marca,
    this.nombre=nombre,
    this.velocidad=velocidad,    
    this.modelo=opciones.modelo,
    this.potencia=opciones.potencia,
    this.nacionalidad=opciones.nacionalidad,
    this.pasajeros=opciones.pasajeros,
    this.puertas=opciones.puertas,
    this.uso=opciones.uso,
        
    
    this.imprimirInfo = function(){
        var info='El Vehiculo es un(a) ' +this.auto +' '+ this.marca + this.nombre +' va a una velocidad de '+ this.velocidad + ' Km/h'
        console.log(info);
    }

    this.estaCorriendo =function  (auto2)
    {
    if(this.velocidad >= 0 && this.velocidad <= 40 ){
        console.log('El '+ this.nombre+ ' va a velocidad baja')
    }
    else 
        if(this.velocidad >= 41 && this.velocidad <= 80 ){
            console.log('El '+ this.nombre+ ' va a velocidad media')
        }
        else 
            if(this.velocidad >= 81 && this.velocidad <= 100 ){
                console.log('El '+ this.nombre+ ' va a velocidad alta')
            }
            else 
                if(this.velocidad >= 101 ){
                    console.log('El '+ this.nombre+ ' va a velocidad muy alta, esta corriendo')
                }
                else
                {
                    console.log('el '+ this.nombre+ ' esta apagado o estacionado')            
                }     
    }
}
    

var opciones = {
    modelo: '2007',
    potencia:'220',
    nacionalidad:'Mexico',
    pasajeros:'4',
    puertas:'4',
    uso:'familiar'
};

var auto = new Vehiculo('automovil','NISSAN ' ,'GTR','320', opciones);
    auto.imprimirInfo();
    auto.estaCorriendo();

var opcionesAutobus={
    modelo: '2017',
    potencia:'600',
    nacionalidad:'EUA',
    pasajeros:'36',
    puertas:'3',
    uso:'Transporte'
}
var autobus = new Vehiculo('Autobus','Scania ' ,'Irizar','88', opciones);
    autobus.imprimirInfo();
    autobus.estaCorriendo();

var opcionesTrailer={
    modelo: '2018',
    potencia:'720',
    nacionalidad:'EUA',
    pasajeros:'3',
    puertas:'3',
    uso:'Carga 10 Toneladas'
}
var trailer = new Vehiculo('Trailer','Volvo ' ,'VNL 860', '0', opciones);
    trailer.imprimirInfo();
    trailer.estaCorriendo();

var opcionesTaxi={
    modelo: '1990',
    potencia:'50',
    nacionalidad:'Mexico',
    pasajeros:'9',
    puertas:'5',
    uso:'Transporte'
}
var taxi = new Vehiculo('taxi','Nissan ' ,'Tsuru', '62',opciones);
    taxi.imprimirInfo();
    taxi.estaCorriendo();

var opcionesMamaMovil={
    modelo: '2019',
    potencia:'250',
    nacionalidad:'Japon',
    pasajeros:'8 y la mascota',
    puertas:'5',
    uso:'familiar'
}
var van = new Vehiculo('van','Toyota ' ,'Sienna', '30',opciones);
    van.imprimirInfo();
    van.estaCorriendo();


/*
var batman= new Personaje('Batman', opciones2)

var superman= new Personaje('Superman',opciones3)
batman.imprimirInfo()

superman.imprimirInfo()


console.log(batman.nombre +" está atacando a " + superman.nombre);
batman.atacarA(superman);
console.log('superman esta vivo?... '+ superman.estavivo())
superman.imprimirInfo()

while(superman.estavivo()){
    batman.atacarA(superman);
    console.log('batman se esta chingando a superman con ataques de ' + batman.fuerza);
    console.log('Superman sigue vivo? ----' + (superman.estavivo()===true ? "Aun sigue Vivo" : "Superman ha muerto por el puñetas del batman"));
    console.log('HP de superman: ' + superman.HP)
    
}


*/




