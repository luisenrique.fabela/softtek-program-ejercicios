var alumnos=[
    {
        nombre:'Pedro Ramos',
        fechaNacimiento:'1/Abril/1998',

    },
    
    {
        nombre:'Veronica Treviño',
        fechaNacimiento:'13/Dic/2000',
    },
    {
        nombre:'Debany Rios',
        fechaNacimiento:'25/Jul/2001',
    },
    {
        nombre:'Daniel Coronado',
        fechaNacimiento:'27/Dic/1999',
    },
    
    {
        nombre:'Juanpa Calvo',
        fechaNacimiento:'24/Jun/2001',
    },
    {
        nombre:'Nathan Flores',
        fechaNacimiento:'01/Dic/2000',
    },
    
    {
        nombre:'Luis Fabela',
        fechaNacimiento:'2/Abril/1992',
    },
    {
        nombre:'Jonathan Sanchez',
        fechaNacimiento:'05/Abril/1999',
    },
    {
        nombre:'Johnathan Rodriguez',
        fechaNacimiento:'23/Octubre/2000',
    }
    
]


var boton = document.getElementById('generar-tabla');

boton.addEventListener('click',function(){
    generarTabla(alumnos)
});


function generarTabla(listaDeAlumnos){
    //seleccionar  elemento body
    var body = document.getElementsByTagName('body')[0];
    
    /************ GENERAR LA TABLA***************/
    var tabla = document.createElement('table')
    
    for(var i = 0; i < alumnos.length; i++){
        //sacar objetos del alumno
        var alumno =listaDeAlumnos[i];
        //crear elementos necesarios
        var tr= document.createElement('tr');
        var tdNombre=document.createElement('td');
        var tdNacimiento= document.createElement('td');
        
        //Añadir valores a los elementos
        tdNombre.innerHTML=alumno.nombre
        tdNacimiento.innerHTML= alumno.fechaNacimiento
        
        //Añadir columnas a la linea
        tr.appendChild(tdNombre);
        tr.appendChild(tdNacimiento);
        
        //añadir linea a la tabla
        
        tabla.appendChild(tr);

        console.log(alumnos[i]);
    }     
    
    tabla.setAttribute('border', '2')
    body.appendChild(tabla);
}














