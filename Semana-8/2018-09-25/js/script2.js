
            $.getJSON('https://jsonplaceholder.typicode.com/users', function(valor){//data es la respuesta del servidor
                console.log(valor);
                
                var personajes= valor; //data.results es un array de todos los personajes
                
                
                for(var i = 0; i < personajes.length; i++){
                    var contenedor = $('.personaje.hidden').clone();/**/
                    var personaje=personajes[i];
                    
                    $(contenedor)
                        .find('#personaje__nombre').text(personaje.name);
                    
                    $(contenedor)
                        .find('#personaje__usuario').text(personaje.username);
                    
                    $(contenedor)
                        .find('#personaje__email').text(personaje.email);
                    
                    $(contenedor)
                        .find('#personaje__direccion').html(personaje.address.street +' ' +personaje.address.suite +' ' +personaje.address.city);
                    
                    $(contenedor)
                        .find('#personaje__telefono').html(personaje.phone);
                    
                    $(contenedor)
                        .find('#personaje__sitioWeb').html(personaje.website);
                    
                    $(contenedor)
                        .find('#personaje__Compañia').html(personaje.company.name);

                    $(contenedor)
                        .removeClass('hidden')
                        .appendTo('body');
                }

                
            });
               
    